# frozen_string_literal: true

require 'rails_helper'

RSpec.describe List, type: :model do
  context '#associations' do
    it { expect(List.reflect_on_association(:list_members).macro).to eq(:has_many) }
    it { expect(List.reflect_on_association(:users).macro).to eq(:has_many) }
    it { expect(List.reflect_on_association(:created_user).macro).to eq(:belongs_to) }
  end

  context '#validations' do
    before do
      user = FactoryBot.create(:user)
      @list = FactoryBot.create(:list, created_user: user)
    end

    subject { @list }

    it { is_expected.to validate_presence_of(:title) }
  end
end
