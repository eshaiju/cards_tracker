# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ListMember, type: :model do
  context '#associations' do
    it { expect(ListMember.reflect_on_association(:list).macro).to eq(:belongs_to) }
    it { expect(ListMember.reflect_on_association(:user).macro).to eq(:belongs_to) }
  end
end
