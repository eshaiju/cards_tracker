# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Card, type: :model do
  context '#associations' do
    it { expect(Card.reflect_on_association(:list).macro).to eq(:belongs_to) }
    it { expect(Card.reflect_on_association(:created_user).macro).to eq(:belongs_to) }
    it { expect(Card.reflect_on_association(:comments).macro).to eq(:has_many) }
  end

  context '#validations' do
    before do
      user = FactoryBot.create(:user)
      list = FactoryBot.create(:list, created_user: user)
      @card = FactoryBot.create(:card, list: list, created_user: user)
    end

    subject { @card }

    it { is_expected.to validate_presence_of(:title) }
  end
end
