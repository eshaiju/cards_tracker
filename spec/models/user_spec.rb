# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  context '#associations' do
    it { expect(User.reflect_on_association(:list_members).macro).to eq(:has_many) }
    it { expect(User.reflect_on_association(:lists).macro).to eq(:has_many) }
    it { expect(User.reflect_on_association(:created_lists).macro).to eq(:has_many) }
    it { expect(User.reflect_on_association(:comments).macro).to eq(:has_many) }
  end

  context '#validations' do
    before { @user = FactoryBot.build(:user) }

    subject { @user }

    it { is_expected.to validate_uniqueness_of(:email).ignoring_case_sensitivity }
    it { is_expected.to validate_uniqueness_of(:username) }
    it { is_expected.to validate_presence_of(:username) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_presence_of(:role) }
  end
end
