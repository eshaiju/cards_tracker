# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Comment, type: :model do
  context '#associations' do
    it { expect(Comment.reflect_on_association(:commentable).macro).to eq(:belongs_to) }
    it { expect(Comment.reflect_on_association(:replies).macro).to eq(:has_many) }
    it { expect(Comment.reflect_on_association(:user).macro).to eq(:belongs_to) }
  end

  context '#validations' do
    before do
      user = FactoryBot.create(:user)
      list = FactoryBot.create(:list, created_user: user)
      card = FactoryBot.create(:card, list: list, created_user: user)
      @comment = FactoryBot.create(:comment, user: user, list: list, commentable: card)
    end

    subject { @comment }

    it { is_expected.to validate_presence_of(:content) }
  end
end
