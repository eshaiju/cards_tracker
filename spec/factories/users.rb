# frozen_string_literal: true

password = Faker::Internet.password

FactoryBot.define do
  factory :user do
    username { Faker::Name.first_name }
    email { Faker::Internet.email }
    role { User::ADMIN }
    password { password }
    password_confirmation { password }
  end
end
