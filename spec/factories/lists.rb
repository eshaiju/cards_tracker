# frozen_string_literal: true

FactoryBot.define do
  factory :list do
    title { Faker::Lorem.word }
    created_user_id { 1 }
  end
end
