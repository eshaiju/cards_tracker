# frozen_string_literal: true

FactoryBot.define do
  factory :list_member do
    list_id { 1 }
    user_id { 1 }
  end
end
