# frozen_string_literal: true

FactoryBot.define do
  factory :comment do
    content { Faker::Lorem.sentence }
    list_id { 1 }
    user_id { 1 }
    commentable_id { '' }
    commentable_type { 'MyString' }
  end
end
