# frozen_string_literal: true

FactoryBot.define do
  factory :card do
    title { Faker::Lorem.sentence }
    description { 'MyText' }
    list_id { 1 }
    created_user_id { 1 }
  end
end
