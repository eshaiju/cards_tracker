# frozen_string_literal: true

require 'swagger_helper'

describe 'Cards API' do
  path '/api/v1/cards' do
    get 'Retrieves all cards of user' do
      tags 'Cards'
      produces 'application/json'
      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )

      response '200', '' do
        let(:user) { FactoryBot.create(:user) }
        let(:Authorization) { JsonWebToken.encode(user_id: user.id) }
        run_test!
      end
    end
  end

  path '/api/v1/cards/{id}' do
    get 'Retrieves details of a card' do
      tags 'Cards'
      produces 'application/json'
      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )
      parameter name: :id, in: :path, type: :string

      response '200', '' do
        let(:user) { FactoryBot.create(:user) }
        let(:list) { FactoryBot.create(:list, created_user: user) }
        let(:card) do
          FactoryBot.create(:card,
                            title: 'Sample Title',
                            list_id: list.id,
                            created_user: user)
        end
        let(:id) { card.id }
        let(:Authorization) { JsonWebToken.encode(user_id: user.id) }
        run_test!
      end
    end
  end

  path '/api/v1/cards' do
    post 'Creates a card' do
      tags 'Cards'
      consumes 'application/json'
      produces 'application/json'

      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )

      parameter name: :card, in: :body, schema: {
        type: :object,
        properties: {
          card: {
            properties: {
              title: { type: :string },
              description: { type: :string },
              list_id: { type: :integer }
            }
          }
        },
        required: %i[title list_id]
      }

      let(:user) { FactoryBot.create(:user) }
      let(:list) { FactoryBot.create(:list, created_user: user) }
      let(:card) do
        FactoryBot.attributes_for(:card,
                                  title: 'Sample Title',
                                  list_id: list.id)
      end
      let(:Authorization) { JsonWebToken.encode(user_id: user.id) }

      response '201', 'user created' do
        run_test!
      end
    end
  end

  path '/api/v1/cards/{id}/' do
    put 'Updates card' do
      tags 'Cards'
      consumes 'application/json'
      produces 'application/json'
      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )

      parameter name: :id, in: :path, type: :string

      parameter name: :card, in: :body, schema: {
        type: :object,
        properties: {
          card: {
            properties: {
              title: { type: :string },
              description: { type: :string },
              list_id: { type: :integer }
            }
          }
        }
      }

      let(:user) { FactoryBot.create(:user) }
      let(:Authorization) { JsonWebToken.encode(user_id: user.id) }
      let(:list) { FactoryBot.create(:list, created_user: user) }
      let(:card) do
        FactoryBot.create(:card,
                          title: 'Sample Title',
                          list_id: list.id,
                          created_user: user)
      end

      response '200', 'ticket updated' do
        let(:id) { card.id }
        run_test!
      end
    end
  end

  path '/api/v1/cards/{id}/' do
    delete 'Delete card' do
      tags 'Cards'
      consumes 'application/json'
      produces 'application/json'
      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )

      parameter name: :id, in: :path, type: :string

      let(:user) { FactoryBot.create(:user) }
      let(:Authorization) { JsonWebToken.encode(user_id: user.id) }
      let(:list) { FactoryBot.create(:list, created_user: user) }
      let(:card) { FactoryBot.create(:card, list: list, created_user: user) }

      response '200', 'card deleted' do
        let(:id) { card.id }
        run_test!
      end
    end
  end
end
