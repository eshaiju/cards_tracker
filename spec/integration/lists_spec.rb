# frozen_string_literal: true

require 'swagger_helper'

describe 'Lists API' do
  path '/api/v1/lists' do
    get 'Retrieves all lists of user' do
      tags 'Lists'
      produces 'application/json'
      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )

      response '200', '' do
        let(:user) { FactoryBot.create(:user) }
        let(:Authorization) { JsonWebToken.encode(user_id: user.id) }
        run_test!
      end
    end
  end

  path '/api/v1/lists/{id}' do
    get 'Retrieves details of a list' do
      tags 'Lists'
      produces 'application/json'
      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )
      parameter name: :id, in: :path, type: :string

      response '200', '' do
        let(:user) { FactoryBot.create(:user) }
        let(:list) { FactoryBot.create(:list, created_user: user) }
        let(:id) { list.id }
        let(:Authorization) { JsonWebToken.encode(user_id: user.id) }
        run_test!
      end
    end
  end

  path '/api/v1/lists' do
    post 'Creates a list' do
      tags 'Lists'
      consumes 'application/json'
      produces 'application/json'

      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )

      parameter name: :list, in: :body, schema: {
        type: :object,
        properties: {
          list: {
            properties: {
              title: { type: :string }
            }
          }
        },
        required: %i[title]
      }

      let(:user) { FactoryBot.create(:user) }
      let(:list) do
        FactoryBot.attributes_for(:list,
                                  title: 'Sample Title',
                                  created_user_id: user.id)
      end
      let(:Authorization) { JsonWebToken.encode(user_id: user.id) }
      response '201', 'user created' do
        run_test!
      end

      response '400', 'bad request' do
        let(:list) do
          FactoryBot.attributes_for(:list,
                                    title: nil,
                                    created_user_id: user.id)
        end
        run_test!
      end
    end
  end

  path '/api/v1/lists/{id}/' do
    put 'Updates list' do
      tags 'Lists'
      consumes 'application/json'
      produces 'application/json'
      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )

      parameter name: :id, in: :path, type: :string

      parameter name: :list, in: :body, schema: {
        type: :object,
        properties: {
          list: {
            properties: {
              title: { type: :string }
            }
          }
        }
      }

      let(:user) { FactoryBot.create(:user) }
      let(:Authorization) { JsonWebToken.encode(user_id: user.id) }
      let(:list) { FactoryBot.create(:list, created_user: user) }

      response '200', 'ticket updated' do
        let(:id) { list.id }
        run_test!
      end
    end
  end

  path '/api/v1/lists/{id}/' do
    delete 'Delete list' do
      tags 'Lists'
      consumes 'application/json'
      produces 'application/json'
      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )

      parameter name: :id, in: :path, type: :string

      let(:user) { FactoryBot.create(:user) }
      let(:Authorization) { JsonWebToken.encode(user_id: user.id) }
      let(:list) { FactoryBot.create(:list, created_user: user) }

      response '200', 'list deleted' do
        let(:id) { list.id }
        run_test!
      end
    end
  end

  path '/api/v1/lists/{list_id}/assign_member' do
    post 'Assign a member to list' do
      tags 'Lists'
      consumes 'application/json'
      produces 'application/json'
      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )

      parameter name: :list_id, in: :path, type: :string, required: true
      parameter name: :member_id, in: :body, schema: {
        type: :object,
        properties: {
          member_id: { type: :integer }
        }
      }

      let(:user) { FactoryBot.create(:user) }
      let(:member) { FactoryBot.create(:user, role: User::MEMBER) }
      let(:Authorization) { JsonWebToken.encode(user_id: user.id) }
      let(:list) { FactoryBot.create(:list, created_user: user) }

      response '200', 'Assigned a member' do
        let(:list_id) { list.id }
        let(:member_id) { member.id }

        run_test!
      end
    end
  end

  path '/api/v1/lists/{list_id}/un_assign_member' do
    post 'Un assign a member from list' do
      tags 'Lists'
      consumes 'application/json'
      produces 'application/json'
      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )

      parameter name: :list_id, in: :path, type: :string, required: true
      parameter name: :member_id, in: :body, schema: {
        type: :object,
        properties: {
          member_id: { type: :integer }
        }
      }

      let(:user) { FactoryBot.create(:user) }
      let(:member_id) { FactoryBot.create(:user, role: User::MEMBER).id }
      let(:Authorization) { JsonWebToken.encode(user_id: user.id) }
      let(:list_id) { FactoryBot.create(:list, created_user: user).id }
      let(:list_member) { FactoryBot.create(:list_member, list_id: list_id, user_id: member_id) }

      response '404', '' do
        run_test!
      end
    end
  end
end
