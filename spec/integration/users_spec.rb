# frozen_string_literal: true

require 'swagger_helper'

describe 'Users API' do
  path '/api/v1/users' do
    get 'Retrieves all users' do
      tags 'Users'
      produces 'application/json'
      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )

      response '200', '' do
        let(:user) { FactoryBot.create(:user, password: '12345678', password_confirmation: '12345678') }
        let(:Authorization) { JsonWebToken.encode(user_id: user.id) }
        run_test!
      end

      response '401', '' do
        let(:Authorization) { '' }
        run_test!
      end
    end
  end

  path '/api/v1/users' do
    post 'Creates a user' do
      tags 'Users'
      consumes 'application/json'
      produces 'application/json'

      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          user: {
            properties: {
              email: { type: :string },
              username: { type: :string },
              role: { type: :string, example: 'admin/member' },
              password: { type: :string }
            }
          }
        },
        required: %i[email username role password]
      }

      let(:user) { { user: FactoryBot.attributes_for(:user) } }
      let(:Authorization) { JsonWebToken.encode(user_id: user.id) }
      response '201', 'user created' do
        run_test!
      end

      response '400', 'bad request' do
        let(:user) { { user: { username: 'example' } } }
        run_test!
      end
    end
  end

  path '/api/v1/users/login' do
    post 'login' do
      tags 'Users'

      consumes 'application/json'
      produces 'application/json'

      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          username: { type: :string },
          password: { type: :string }
        },
        required: %i[email password]
      }

      response '200', 'created' do
        before do
          @user = FactoryBot.create(:user, password: '12345678', password_confirmation: '12345678')
        end

        let(:user) { { username: @user.username, password: @user.password } }

        run_test!
      end
    end
  end
end
