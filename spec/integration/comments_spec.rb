# frozen_string_literal: true

require 'swagger_helper'

describe 'Comments API' do
  path '/api/v1/comments' do
    get 'Retrieves all comments of user' do
      tags 'Comments'
      produces 'application/json'
      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )

      parameter name: :resource_type, in: :query, type: :string, optional: true, description: 'Card/Comment'
      parameter name: :page, in: :query, type: :string, optional: true, description: '1 or grater'

      response '200', '' do
        let(:user) { FactoryBot.create(:user) }
        let(:card) do
          FactoryBot.create(:card,
                            title: 'Sample Title',
                            list_id: list.id,
                            created_user: user)
        end
        let(:comment) do
          FactoryBot.create(:comment,
                            user: user,
                            content: 'Sample Title',
                            commentable_id: card.id,
                            commentable_type: 'Card')
        end
        let(:Authorization) { JsonWebToken.encode(user_id: user.id) }
        let(:resource_type) {}
        let(:page) {}
        run_test!
      end
    end
  end

  path '/api/v1/comments/{id}' do
    get 'Retrieves details of a comment' do
      tags 'Comments'
      produces 'application/json'
      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )
      parameter name: :id, in: :path, type: :string

      response '200', '' do
        let(:user) { FactoryBot.create(:user) }
        let(:list) { FactoryBot.create(:list, created_user: user) }
        let(:card) do
          FactoryBot.create(:card,
                            title: 'Sample Title',
                            list_id: list.id,
                            created_user: user)
        end
        let(:comment) do
          FactoryBot.create(:comment,
                            user: user,
                            content: 'Sample Title',
                            commentable_id: card.id,
                            commentable_type: 'Card')
        end
        let(:id) { comment.id }
        let(:Authorization) { JsonWebToken.encode(user_id: user.id) }
        run_test!
      end
    end
  end
  path '/api/v1/comments' do
    post 'Creates a comment' do
      tags 'Comments'
      consumes 'application/json'
      produces 'application/json'

      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )

      parameter name: :comment, in: :body, schema: {
        type: :object,
        properties: {
          comment: {
            properties: {
              content: { type: :string },
              commentable_id: { type: :string },
              commentable_type: { type: :string, example: 'Card/Comment' }
            }
          }
        },
        required: %i[content commentable_id commentable_type]
      }

      let(:user) { FactoryBot.create(:user) }
      let(:list) { FactoryBot.create(:list, created_user: user) }
      let(:card) { FactoryBot.create(:card, list: list, created_user: user) }
      let(:comment) do
        FactoryBot.attributes_for(:comment,
                                  content: 'Sample Title',
                                  commentable_id: card.id,
                                  commentable_type: 'Card')
      end
      let(:Authorization) { JsonWebToken.encode(user_id: user.id) }

      response '201', 'user created' do
        run_test!
      end
    end
  end

  path '/api/v1/comments/{id}/' do
    put 'Updates comment' do
      tags 'Comments'
      consumes 'application/json'
      produces 'application/json'
      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )

      parameter name: :id, in: :path, type: :string

      parameter name: :comment, in: :body, schema: {
        type: :object,
        properties: {
          comment: {
            properties: {
              content: { type: :string },
              commentable_id: { type: :string },
              commentable_type: { type: :string, example: 'Card/Comment' }
            }
          }
        }
      }

      let(:user) { FactoryBot.create(:user) }
      let(:Authorization) { JsonWebToken.encode(user_id: user.id) }
      let(:list) { FactoryBot.create(:list, created_user: user) }
      let(:card) { FactoryBot.create(:card, list: list, created_user: user) }
      let(:comment) do
        FactoryBot.create(:comment,
                          user: user,
                          content: 'Sample Title',
                          commentable_id: card.id,
                          commentable_type: 'Card')
      end

      response '200', 'ticket updated' do
        let(:id) { comment.id }
        run_test!
      end
    end
  end

  path '/api/v1/comments/{id}/' do
    delete 'Delete comment' do
      tags 'Comments'
      consumes 'application/json'
      produces 'application/json'
      parameter(
        in: :header,
        type: :string,
        name: 'Authorization',
        required: true,
        description: 'Authentication token'
      )

      parameter name: :id, in: :path, type: :string

      let(:user) { FactoryBot.create(:user) }
      let(:Authorization) { JsonWebToken.encode(user_id: user.id) }
      let(:list) { FactoryBot.create(:list, created_user: user) }
      let(:card) { FactoryBot.create(:card, list: list, created_user: user) }
      let(:comment) do
        FactoryBot.create(:comment,
                          user: user,
                          content: 'Sample Title',
                          commentable_id: card.id,
                          commentable_type: 'Card')
      end

      response '200', 'comment deleted' do
        let(:id) { comment.id }
        run_test!
      end
    end
  end
end
