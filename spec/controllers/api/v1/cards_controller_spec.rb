# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::CardsController do
  before do
    @user = FactoryBot.create(:user, role: User::ADMIN)
    @list = FactoryBot.create(:list, created_user: @user)
    api_authorization_header JsonWebToken.encode(user_id: @user.id)
  end

  describe 'GET #index' do
    before do
      @card = FactoryBot.create(:card,
                                list_id: @list.id,
                                created_user: @user)
      get :index, params: {}, format: :json
    end

    it 'returns the lists of user' do
      expect(json_response[:cards][:data][0][:attributes][:title]).to eql @card.title
    end

    it { is_expected.to respond_with 200 }
  end

  describe 'GET #show' do
    before do
      @card = FactoryBot.create(:card,
                                list_id: @list.id,
                                created_user: @user)
      4.times { FactoryBot.create(:comment, user: @user, commentable: @card) }
      get :show, params: { id: @card.id }, format: :json
    end

    it 'returns the informations of card' do
      expect(json_response[:card][:data][:attributes][:title]).to eql @card.title
    end

    it 'returns first 3 comments' do
      expect(json_response[:card][:data][:attributes][:first_three_comments][:data].count).to eq 3
    end

    it { is_expected.to respond_with 200 }
  end

  describe 'POST #create' do
    context 'when is successfully created' do
      before do
        @card_attributes = FactoryBot.attributes_for(:card,
                                                     title: 'Sample Title',
                                                     list_id: @list.id)

        post :create, params: { card: @card_attributes }
      end

      it 'renders the json representation for the card record just created' do
        expect(json_response[:card][:data][:attributes][:title]).to eql @card_attributes[:title]
      end

      it { is_expected.to respond_with 201 }
    end

    context 'when is not created' do
      before do
        @invalid_card_attributes = FactoryBot.attributes_for(:card, title: nil, list_id: @list.id)
        post :create, params: { card: @invalid_card_attributes }, format: :json
      end

      it 'renders an errors json' do
        expect(json_response).to have_key(:errors)
      end

      it 'renders the json errors on why the card could not be created' do
        expect(json_response[:errors]).to include "Title can't be blank"
      end

      it { is_expected.to respond_with 400 }
    end

    context 'autorization' do
      context 'admin' do
        before do
          @card_attributes = FactoryBot.attributes_for(:card,
                                                       title: 'Sample Title',
                                                       list_id: @list.id)

          post :create, params: { card: @card_attributes }
        end

        it { is_expected.to respond_with 201 }
      end

      context 'member who part of list' do
        before do
          member = FactoryBot.create(:user, role: User::MEMBER)
          FactoryBot.create(:list_member, list: @list, user: member)

          api_authorization_header JsonWebToken.encode(user_id: member.id)
          @card_attributes = FactoryBot.attributes_for(:card,
                                                       title: 'Sample Title',
                                                       list_id: @list.id)

          post :create, params: { card: @card_attributes }
        end

        it { is_expected.to respond_with 201 }
      end
    end
  end

  describe 'PUT/PATCH #update' do
    before do
      @list = FactoryBot.create(:list,
                                created_user: @user)
      @card = FactoryBot.create(:card,
                                list_id: @list.id,
                                created_user: @user)
    end

    context 'when is successfully updated' do
      before do
        patch :update, params: { id: @card.id,
                                 card: { title: 'updated title' } }, format: :json
        @card.reload
      end

      it 'renders the json representation for the updated card' do
        expect(json_response[:card][:data][:attributes][:title]).to eql 'updated title'
      end

      it { is_expected.to respond_with 200 }
    end

    context 'when is not created' do
      before do
        patch :update, params: { id: @card.id,
                                 card: { title: nil } }, format: :json
      end

      it 'renders an errors json' do
        expect(json_response).to have_key(:errors)
      end

      it 'renders the json errors on whye the card could not be updated' do
        expect(json_response[:errors]).to include "Title can't be blank"
      end

      it { is_expected.to respond_with 400 }
    end

    context 'autorization' do
      context 'admin who created card' do
        before do
          patch :update, params: { id: @card.id,
                                   card: { title: 'updated title' } }, format: :json
          @card.reload
        end

        it { is_expected.to respond_with 200 }
      end

      context 'admin who not created list' do
        before do
          admin_new = FactoryBot.create(:user, role: User::ADMIN)
          api_authorization_header JsonWebToken.encode(user_id: admin_new.id)

          patch :update, params: { id: @card.id,
                                   card: { title: 'updated title' } }, format: :json
          @card.reload
        end

        it { is_expected.to respond_with 401 }
      end

      context 'member who created card' do
        before do
          member = FactoryBot.create(:user, role: User::MEMBER)
          api_authorization_header JsonWebToken.encode(user_id: member.id)
          @card_new = FactoryBot.create(:card,
                                        list_id: @list.id,
                                        created_user: member)
          patch :update, params: { id: @card_new.id,
                                   card: { title: 'updated title' } }, format: :json
          @card.reload
        end

        it { is_expected.to respond_with 200 }
      end
    end
  end

  describe 'DELETE #destroy' do
    before do
      @list = FactoryBot.create(:list, created_user: @user)
      @card = FactoryBot.create(:card,
                                list_id: @list.id,
                                created_user: @user)
    end

    context 'when is successfully deleted' do
      before do
        delete :destroy, params: { id: @card.id }, format: :json
      end

      it { is_expected.to respond_with 200 }
    end

    context 'when unauthorized' do
      before do
        @another_user = FactoryBot.create(:user, username: 'another_user')
      end

      before do
        api_authorization_header JsonWebToken.encode(user_id: @another_user.id)
        delete :destroy, params: { id: @card.id }, format: :json
      end

      it 'renders an errors json' do
        expect(json_response).to have_key(:error)
      end

      it 'renders the json errors on why the card can not be deleted' do
        expect(json_response[:error]).to eq 'Unauthorized'
      end

      it { is_expected.to respond_with 401 }
    end
  end
end
