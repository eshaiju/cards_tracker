# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::UsersController do
  before(:each) do
    @user = FactoryBot.create(:user, password: '12345678', password_confirmation: '12345678')
  end

  describe 'GET #index' do
    before(:each) do
      api_authorization_header JsonWebToken.encode(user_id: @user.id)
      get :index, format: :json
    end

    it 'returns the information about all users' do
      expect(json_response[:users][:data][0][:attributes][:email]).to eql @user.email
    end

    it { is_expected.to respond_with 200 }
  end

  describe 'POST #create' do
    context 'when is successfully created' do
      before(:each) do
        @user_attributes = FactoryBot.attributes_for :user
        post :create, params: { user: @user_attributes }
      end

      it 'renders the json representation for the user record just created' do
        expect(json_response[:user][:data][:attributes][:email]).to eql @user_attributes[:email]
      end

      it { is_expected.to respond_with 201 }
    end

    context 'when is not created' do
      before(:each) do
        @invalid_user_attributes = { password: '12345678',
                                     password_confirmation: '12345678' }
        post :create, params: { user: @invalid_user_attributes }, format: :json
      end

      it 'renders an errors json' do
        expect(json_response).to have_key(:errors)
      end

      it 'renders the json errors on why the user could not be created' do
        expect(json_response[:errors]).to include "Email can't be blank"
      end

      it { is_expected.to respond_with 400 }
    end
  end

  describe 'POST #login' do
    context 'when the credentials are correct' do
      before(:each) do
        credentials = { username: @user.username, password: '12345678' }
        post :login, params: credentials
      end

      it 'returns the user record corresponding to the given credentials' do
        @user.reload
        auth_token = JsonWebToken.encode(user_id: @user.id)
        expect(json_response[:auth_token]).to eql auth_token
      end

      it { should respond_with 200 }
    end

    context 'when the credentials are incorrect' do
      before(:each) do
        credentials = { username: @user.username, password: 'invalidpassword' }
        post :login, params: credentials
      end

      it 'returns a json with an error' do
        expect(json_response[:error]).to eql 'Invalid username/password'
      end

      it { should respond_with 401 }
    end
  end
end
