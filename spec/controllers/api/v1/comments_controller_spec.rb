# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::CommentsController do
  before do
    @user = FactoryBot.create(:user, role: User::ADMIN)
    @list = FactoryBot.create(:list, created_user: @user)
    @card = FactoryBot.create(:card, list: @list, created_user: @user)
    api_authorization_header JsonWebToken.encode(user_id: @user.id)
  end

  describe 'GET #index' do
    before do
      @comment = FactoryBot.create(:comment,
                                   content: 'Sample comment',
                                   list_id: @list.id,
                                   user: @user,
                                   commentable_id: @card.id,
                                   commentable_type: 'Card')
      get :index, params: {}, format: :json
    end

    it 'returns the comments of user' do
      expect(json_response[:comments][:data][0][:attributes][:content]).to eql @comment.content
    end

    it { is_expected.to respond_with 200 }
  end

  describe 'GET #show' do
    before do
      @comment = FactoryBot.create(:comment,
                                   content: 'Sample comment',
                                   list_id: @list.id,
                                   user: @user,
                                   commentable_id: @card.id,
                                   commentable_type: 'Card')
      @reply = FactoryBot.create(:comment,
                                  content: 'Reply comment',
                                  list_id: @list.id,
                                  user: @user,
                                  commentable_id: @comment.id,
                                  commentable_type: 'Comment')
      get :show, params: { id: @comment.id }, format: :json
    end

    it 'returns the information of comment' do
      expect(json_response[:comment][:data][:attributes][:content]).to eql @comment.content
    end

    it 'returns the replies of comment' do
      expect(json_response[:comment][:data][:attributes][:replies][:data][0][:attributes][:id]).to eql @reply.id
    end

    it { is_expected.to respond_with 200 }
  end

  describe 'POST #create' do
    context 'when is successfully created' do
      before do
        @comment_attributes = FactoryBot.attributes_for(:comment,
                                                        content: 'Sample comment',
                                                        commentable_id: @card.id,
                                                        commentable_type: 'Card',
                                                        list_id: @list.id)

        post :create, params: { comment: @comment_attributes }
      end

      it 'renders the json representation for the comment record just created' do
        expect(json_response[:comment][:data][:attributes][:content]).to eql @comment_attributes[:content]
      end

      it { is_expected.to respond_with 201 }
    end

    context 'when is not created' do
      before do
        @invalid_comment_attributes = FactoryBot.attributes_for(:card, content: nil, list_id: @list.id)
        post :create, params: { comment: @invalid_comment_attributes }, format: :json
      end

      it 'renders an errors json' do
        expect(json_response).to have_key(:errors)
      end

      it 'renders the json errors on why the comment could not be created' do
        expect(json_response[:errors]).to include "Content can't be blank"
      end

      it { is_expected.to respond_with 400 }
    end

    context 'autorization' do
      context 'admin' do
        before do
          @comment_attributes = FactoryBot.attributes_for(:comment,
                                                          content: 'Sample comment',
                                                          commentable_id: @card.id,
                                                          commentable_type: 'Card',
                                                          list_id: @list.id)

          post :create, params: { comment: @comment_attributes }
        end

        it { is_expected.to respond_with 201 }
      end

      context 'member who part of list' do
        before do
          member = FactoryBot.create(:user, role: User::MEMBER)
          FactoryBot.create(:list_member, list: @list, user: member)

          api_authorization_header JsonWebToken.encode(user_id: member.id)
          @comment_attributes = FactoryBot.attributes_for(:comment,
                                                          content: 'Sample comment',
                                                          commentable_id: @card.id,
                                                          commentable_type: 'Card',
                                                          list_id: @list.id)

          post :create, params: { comment: @comment_attributes }
        end

        it { is_expected.to respond_with 201 }
      end
    end
  end

  describe 'PUT/PATCH #update' do
    before do
      @comment = FactoryBot.create(:comment,
                                   content: 'Sample Title',
                                   list_id: @list.id,
                                   user: @user,
                                   commentable_id: @card.id,
                                   commentable_type: 'Card')
    end

    context 'when is successfully updated' do
      before do
        patch :update, params: { id: @comment.id,
                                 comment: { content: 'updated content' } }, format: :json
        @comment.reload
      end

      it 'renders the json representation for the updated comment' do
        expect(json_response[:comment][:data][:attributes][:content]).to eql 'updated content'
      end

      it { is_expected.to respond_with 200 }
    end

    context 'when is not created' do
      before do
        patch :update, params: { id: @comment.id,
                                 comment: { content: nil } }, format: :json
      end

      it 'renders an errors json' do
        expect(json_response).to have_key(:errors)
      end

      it 'renders the json errors on whye the comment could not be updated' do
        expect(json_response[:errors]).to include "Content can't be blank"
      end

      it { is_expected.to respond_with 400 }
    end

    context 'autorization' do
      context 'admin who created comment' do
        before do
          patch :update, params: { id: @comment.id,
                                   comment: { content: 'updated content' } }, format: :json
          @comment.reload
        end

        it { is_expected.to respond_with 200 }
      end

      context 'admin who not created list' do
        before do
          admin_new = FactoryBot.create(:user, role: User::ADMIN)
          api_authorization_header JsonWebToken.encode(user_id: admin_new.id)

          patch :update, params: { id: @comment.id,
                                   comment: { content: 'updated content' } }, format: :json
          @comment.reload
        end

        it { is_expected.to respond_with 401 }
      end

      context 'member who created comment' do
        before do
          member = FactoryBot.create(:user, role: User::MEMBER)
          api_authorization_header JsonWebToken.encode(user_id: member.id)

          FactoryBot.create(:list_member, list: @list, user: member)
          @card_new = FactoryBot.create(:card,
                                        list_id: @list.id,
                                        created_user: member)
          @comment_new = FactoryBot.create(:comment,
                                           content: 'Sample Title',
                                           list_id: @list.id,
                                           user: member,
                                           commentable_id: @card_new.id,
                                           commentable_type: 'Card')

          patch :update, params: { id: @comment_new.id,
                                   comment: { content: 'updated content' } }, format: :json
          @comment_new.reload
        end

        it { is_expected.to respond_with 200 }
      end
    end
  end

  describe 'DELETE #destroy' do
    before do
      @comment = FactoryBot.create(:comment,
                                   content: 'Sample Title',
                                   user: @user,
                                   commentable_id: @card.id,
                                   commentable_type: 'Card')
    end

    context 'when is successfully deleted' do
      before do
        delete :destroy, params: { id: @comment.id }, format: :json
      end

      it { is_expected.to respond_with 200 }
    end

    context 'when unauthorized' do
      before do
        @another_user = FactoryBot.create(:user, username: 'another_user')
      end

      before do
        api_authorization_header JsonWebToken.encode(user_id: @another_user.id)
        delete :destroy, params: { id: @comment.id }, format: :json
      end

      it 'renders an errors json' do
        expect(json_response).to have_key(:error)
      end

      it 'renders the json errors on why the comment can not be deleted' do
        expect(json_response[:error]).to eq 'Unauthorized'
      end

      it { is_expected.to respond_with 401 }
    end
  end
end
