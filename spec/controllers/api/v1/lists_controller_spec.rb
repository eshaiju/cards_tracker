# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::ListsController do
  before do
    @user = FactoryBot.create(:user, role: User::ADMIN)
    api_authorization_header JsonWebToken.encode(user_id: @user.id)
  end

  describe 'GET #index' do
    before do
      @list = FactoryBot.create(:list,
                                created_user: @user)
      get :index, params: {}, format: :json
    end

    it 'returns the information lists of user' do
      expect(json_response[:lists][:data][0][:attributes][:title]).to eql @list.title
    end

    it { is_expected.to respond_with 200 }
  end

  describe 'GET #show' do
    before do
      @list = FactoryBot.create(:list,
                                created_user: @user)
      @card = FactoryBot.create(:card,
                                list_id: @list.id,
                                created_user: @user)
      @another_card = FactoryBot.create(:card,
                                        list_id: @list.id,
                                        created_user: @user)
      2.times { FactoryBot.create(:comment, user: @user, commentable: @card) }
      4.times { FactoryBot.create(:comment, user: @user, commentable: @another_card) }
      get :show, params: { id: @list.id }, format: :json
    end

    it 'returns the informations of list' do
      expect(json_response[:list][:data][:attributes][:title]).to eql @list.title
    end

    it 'returns cards in descenting order of comments count' do
      expect(json_response[:list][:data][:attributes][:cards][:data][0][:id]).to eq @another_card.id.to_s
    end

    it { is_expected.to respond_with 200 }
  end

  describe 'POST #create' do
    context 'when is successfully created' do
      before do
        @list_attributes = FactoryBot.attributes_for(:list,
                                                     title: 'Sample Title',
                                                     created_user_id: @user.id)

        post :create, params: { list: @list_attributes }
      end

      it 'renders the json representation for the list record just created' do
        expect(json_response[:list][:data][:attributes][:title]).to eql @list_attributes[:title]
      end

      it { is_expected.to respond_with 201 }
    end

    context 'when is not created' do
      before do
        @invalid_list_attributes = FactoryBot.attributes_for(:list, title: nil)
        post :create, params: { list: @invalid_list_attributes }, format: :json
      end

      it 'renders an errors json' do
        expect(json_response).to have_key(:errors)
      end

      it 'renders the json errors on why the user could not be created' do
        expect(json_response[:errors]).to include "Title can't be blank"
      end

      it { is_expected.to respond_with 400 }
    end

    context 'autorization' do
      context 'admin' do
        before do
          @list_attributes = FactoryBot.attributes_for(:list,
                                                       title: 'Sample Title',
                                                       created_user_id: @user.id)

          post :create, params: { list: @list_attributes }
        end

        it { is_expected.to respond_with 201 }
      end

      context 'member' do
        before do
          member = FactoryBot.create(:user, role: User::MEMBER)
          api_authorization_header JsonWebToken.encode(user_id: member.id)
          @list_attributes = FactoryBot.attributes_for(:list,
                                                       title: 'Sample Title',
                                                       created_user_id: member.id)

          post :create, params: { list: @list_attributes }
        end

        it { is_expected.to respond_with 401 }
      end
    end
  end

  describe 'PUT/PATCH #update' do
    before do
      @list = FactoryBot.create(:list,
                                created_user: @user)
    end

    context 'when is successfully updated' do
      before do
        patch :update, params: { id: @list.id,
                                 list: { title: 'updated title' } }, format: :json
        @list.reload
      end

      it 'renders the json representation for the updated list' do
        expect(json_response[:list][:data][:attributes][:title]).to eql 'updated title'
      end

      it { is_expected.to respond_with 200 }
    end

    context 'when is not created' do
      before do
        patch :update, params: { id: @list.id,
                                 list: { title: nil } }, format: :json
      end

      it 'renders an errors json' do
        expect(json_response).to have_key(:errors)
      end

      it 'renders the json errors on whye the list could not be created' do
        expect(json_response[:errors]).to include "Title can't be blank"
      end

      it { is_expected.to respond_with 400 }
    end

    context 'autorization' do
      context 'admin who created list' do
        before do
          patch :update, params: { id: @list.id,
                                   list: { title: 'updated title' } }, format: :json
          @list.reload
        end

        it { is_expected.to respond_with 200 }
      end

      context 'admin who not created list' do
        before do
          admin_new = FactoryBot.create(:user, role: User::ADMIN)
          api_authorization_header JsonWebToken.encode(user_id: admin_new.id)

          patch :update, params: { id: @list.id,
                                   list: { title: 'updated title' } }, format: :json
          @list.reload
        end

        it { is_expected.to respond_with 401 }
      end

      context 'member' do
        before do
          member = FactoryBot.create(:user, role: User::MEMBER)
          api_authorization_header JsonWebToken.encode(user_id: member.id)
          patch :update, params: { id: @list.id,
                                   list: { title: 'updated title' } }, format: :json
          @list.reload
        end

        it { is_expected.to respond_with 401 }
      end
    end
  end

  describe 'DELETE #destroy' do
    before do
      @list = FactoryBot.create(:list, created_user: @user)
    end

    context 'when is successfully deleted' do
      before do
        delete :destroy, params: { id: @list.id }, format: :json
      end

      it { is_expected.to respond_with 200 }
    end

    context 'when unauthorized' do
      before do
        @list = FactoryBot.create(:list, created_user: @user)
        @another_user = FactoryBot.create(:user, username: 'another_user')
      end

      before do
        api_authorization_header JsonWebToken.encode(user_id: @another_user.id)
        delete :destroy, params: { id: @list.id }, format: :json
      end

      it 'renders an errors json' do
        expect(json_response).to have_key(:error)
      end

      it 'renders the json errors on why the list can not be created' do
        expect(json_response[:error]).to eq 'Unauthorized'
      end

      it { is_expected.to respond_with 401 }
    end
  end

  describe 'Assign Members' do
    before do
      @member = FactoryBot.create(:user, role: User::MEMBER)
      @list = FactoryBot.create(:list, created_user: @user)
    end

    context 'when is successfully assigned' do
      before do
        post :assign_member, params: { list_id: @list.id, member_id: @member.id }, format: :json
      end

      it { is_expected.to respond_with 200 }
    end

    context 'member already assigned' do
      before do
        @member = FactoryBot.create(:user, role: User::MEMBER)
        @list = FactoryBot.create(:list, created_user: @user)
        FactoryBot.create(:list_member, list: @list, user: @member)
        post :assign_member, params: { list_id: @list.id, member_id: @member.id }, format: :json
      end

      it { is_expected.to respond_with 400 }
    end

    context 'when unauthorized' do
      before do
        @list = FactoryBot.create(:list, created_user: @user)
        @another_user = FactoryBot.create(:user)
        @member = FactoryBot.create(:user, role: User::MEMBER)
      end

      before do
        api_authorization_header JsonWebToken.encode(user_id: @another_user.id)
        delete :assign_member, params: { list_id: @list.id, member_id: @member.id }, format: :json
      end

      it 'renders an errors json' do
        expect(json_response).to have_key(:error)
      end

      it 'renders the json errors on why the member could not be assigned' do
        expect(json_response[:error]).to eq 'Unauthorized'
      end

      it { is_expected.to respond_with 401 }
    end
  end

  describe 'Un Assign Members' do
    before do
      @member = FactoryBot.create(:user, role: User::MEMBER)
      @list = FactoryBot.create(:list, created_user: @user)
      FactoryBot.create(:list_member, list: @list, user: @member)
    end

    context 'when is successfully un assigned' do
      before do
        post :un_assign_member, params: { list_id: @list.id, member_id: @member.id }, format: :json
      end

      it { is_expected.to respond_with 200 }
    end

    context 'member not assigned' do
      before do
        @member = FactoryBot.create(:user, role: User::MEMBER)
        @list = FactoryBot.create(:list, created_user: @user)
        post :un_assign_member, params: { list_id: @list.id, member_id: @member.id }, format: :json
      end

      it { is_expected.to respond_with 404 }
    end

    context 'when unauthorized' do
      before do
        @list = FactoryBot.create(:list, created_user: @user)
        @another_user = FactoryBot.create(:user)
        @member = FactoryBot.create(:user, role: User::MEMBER)
      end

      before do
        api_authorization_header JsonWebToken.encode(user_id: @another_user.id)
        delete :un_assign_member, params: { list_id: @list.id, member_id: @member.id }, format: :json
      end

      it 'renders an errors json' do
        expect(json_response).to have_key(:error)
      end

      it 'renders the json errors on why the member could not be un assigned' do
        expect(json_response[:error]).to eq 'Unauthorized'
      end

      it { is_expected.to respond_with 401 }
    end
  end
end
