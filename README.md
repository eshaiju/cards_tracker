## CARDS-TRACKER

[![Build Status](https://gitlab.com/eshaiju/cards_tracker/badges/master/pipeline.svg)](https://gitlab.com/eshaiju/cards_tracker/pipelines)

Application to track cards under lists 

### Developer Setup

1. Install Ruby 2.6.1 (It is suggested to use a Ruby version manager such as [rbenv](https://github.com/rbenv/rbenv#installation) and then to [install Ruby 2.6](https://github.com/rbenv/rbenv#installing-ruby-versions)).
2. Install Bundler to manager dependencies: `gem install bundler`
3. Setup the database: `bundle exec rake db:migrate`
4. Delete your master.key and credentials.yml.enc files if any then ran: `bin/rails credentials:edit`
5. Start the application: `bundle exec rails s`

### ER Diagram
![alt ER Diagram](public/cards_tracker.png)

### Swagger API Doc
[Swagger Doc](https://cards-tracker.herokuapp.com/api-docs/index.html)

### Commands
- `bundle exec rspec` - Run the full Rspec tests.
- `rake rswag:specs:swaggerize` - Commands for generate swagger docs
