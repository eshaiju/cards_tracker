# frozen_string_literal: true

class User < ApplicationRecord
  ADMIN = 'admin'
  MEMBER = 'member'

  has_secure_password

  has_many :list_members, dependent: :destroy
  has_many :lists, through: :list_members, dependent: :destroy
  has_many :cards, foreign_key: :created_user_id, dependent: :destroy
  has_many :created_lists, class_name: 'List', foreign_key: :created_user_id
  has_many :comments, dependent: :destroy

  validates_presence_of :email, :username, :role
  validates_uniqueness_of :username, :email
  validates_format_of :email, with: /@/

  def admin?
    role == ADMIN
  end

  def member?
    role == MEMBER
  end
end
