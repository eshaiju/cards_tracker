# frozen_string_literal: true

class Card < ApplicationRecord
  belongs_to :created_user, class_name: 'User'
  belongs_to :list
  has_many :comments, as: :commentable, dependent: :destroy

  validates_presence_of :title

  def first_three_comments
    comments.limit(3)
  end
end
