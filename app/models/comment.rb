# frozen_string_literal: true

class Comment < ApplicationRecord
  PER_PAGE = 25

  belongs_to :user
  belongs_to :list
  belongs_to :commentable, polymorphic: true, counter_cache: true
  has_many :replies, as: :commentable, class_name: 'Comment', dependent: :destroy

  validates_presence_of :content

  before_validation :set_list_id

  scope :of_type, ->(type) { where(commentable_type: type) }
  scope :with_pagination, ->(page) { limit(PER_PAGE).offset(((page.to_i - 1).abs * PER_PAGE)) }

  def set_list_id
    self.list_id = commentable&.list_id
  end
end
