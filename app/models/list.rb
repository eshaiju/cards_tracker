# frozen_string_literal: true

class List < ApplicationRecord
  belongs_to :created_user, class_name: 'User'

  has_many :list_members, dependent: :destroy
  has_many :cards, dependent: :destroy
  has_many :users, through: :list_members

  validates_presence_of :title

  def member?(member_id)
    users.pluck(:id).include?(member_id.to_i)
  end

  def cards_order_by_comments_count
    cards.order(comments_count: :desc)
  end
end
