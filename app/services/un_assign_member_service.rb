# frozen_string_literal: true

class UnAssignMemberService
  class MemberNotExistError < StandardError; end
  attr_reader :params, :list

  def initialize(list, params)
    @list = list
    @params = params
  end

  def call
    check_for_member
    remove_member
  end

  private

  def remove_member
    list
      .list_members
      .find_by(user_id: member_id.to_i)
      .destroy
  end

  def check_for_member
    raise MemberNotExistError, 'Member not assigned' unless assigned?
  end

  def assigned?
    list.member?(member_id.to_i)
  end

  def member_id
    params[:member_id]
  end
end
