# frozen_string_literal: true

class AssignMemberService
  class MemberDuplicateError < StandardError; end
  attr_reader :params, :list

  def initialize(list, params)
    @list = list
    @params = params
  end

  def call
    check_duplicate_member
    save_member
  end

  private

  def save_member
    list
      .list_members
      .build(user_id: member_id)
      .save
  end

  def check_duplicate_member
    raise MemberDuplicateError, 'Member already exist' if duplicate?
  end

  def duplicate?
    list.member?(member_id)
  end

  def member_id
    params[:member_id]
  end
end
