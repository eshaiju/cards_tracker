# frozen_string_literal: true

class ListPolicy
  attr_reader :current_user, :resource

  def initialize(current_user, resource)
    @current_user = current_user
    @resource = resource
  end

  def index?
    current_user.present?
  end

  def show?
    admin? || list_member?
  end

  def create?
    admin?
  end

  def update?
    create? && resource.created_user_id == current_user.id
  end

  def destroy?
    update?
  end

  def assign_member?
    update?
  end

  def un_assign_member?
    update?
  end

  private

  def admin?
    current_user.admin?
  end

  def list_member?
    resource.member?(current_user.id)
  end
end
