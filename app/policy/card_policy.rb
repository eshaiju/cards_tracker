# frozen_string_literal: true

class CardPolicy
  attr_reader :current_user, :resource

  def initialize(current_user, resource)
    @current_user = current_user
    @resource = resource
  end

  def show?
    current_user.admin? || list_member?
  end

  def create?
    list_owner? || list_member?
  end

  def update?
    resource.created_user_id == current_user.id
  end

  def destroy?
    list_owner? || list_member?
  end

  private

  def list_member?
    resource&.list&.member?(current_user.id)
  end

  def list_owner?
    current_user.admin? && resource&.list&.created_user_id == current_user.id
  end
end
