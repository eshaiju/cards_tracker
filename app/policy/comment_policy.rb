# frozen_string_literal: true

class CommentPolicy
  attr_reader :current_user, :resource

  def initialize(current_user, resource)
    @current_user = current_user
    @resource = resource
  end

  def show?
    admin? || list_member?
  end

  def create?
    admin? || list_member?
  end

  def update?
    list_owner? || resource.user_id == current_user.id
  end

  def destroy?
    list_owner? || resource.user_id == current_user.id
  end

  private

  def list_member?
    resource&.list&.member?(current_user.id)
  end

  def admin?
    current_user.admin?
  end

  def list_owner?
    admin? && resource&.list&.created_user_id == current_user.id
  end
end
