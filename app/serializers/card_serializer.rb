# frozen_string_literal: true

class CardSerializer < BaseSerializer
  attributes :id, :title, :description, :created_user_id, :list_id

  attribute :first_three_comments, if: proc { |_record, params| include_model?(params, :first_three_comments) } do |object|
    CommentSerializer.new(object.first_three_comments, fields: { comment: %i[id content user_id] })
  end
end
