# frozen_string_literal: true

class ListSerializer < BaseSerializer
  attributes :id, :title, :created_user_id

  attribute :cards, if: proc { |_record, params| include_model?(params, :cards) } do |object|
    CardSerializer.new(object.cards_order_by_comments_count, fields: { card: %i[id title created_user_id] })
  end
end
