# frozen_string_literal: true

class UserSerializer < BaseSerializer
  attributes :id, :username, :role, :email
end
