# frozen_string_literal: true

class CommentSerializer < BaseSerializer
  attributes :id, :content, :commentable_id, :commentable_type, :user_id, :list_id

  attribute :replies, if: proc { |_record, params| include_model?(params, :replies) } do |object|
    CommentSerializer.new(object.replies, fields: { comment: %i[id content user_id] })
  end
end
