# frozen_string_literal: true

# User controller
class Api::V1::CommentsController < ApplicationController
  before_action :authenticate_request!
  before_action :set_comment, except: %i[create index]

  def index
    comments = @current_user.comments
    comments = comments.of_type(params[:resource_type]) if params[:resource_type].present?
    comments = comments.with_pagination(params[:page]) if paginate?

    render json: { comments: CommentSerializer.new(comments) }, status: :ok
  end

  def show
    return invalid_authentication unless comment_policy.show?

    render json: { comment: CommentSerializer.new(@comment, params: { include: [:replies] }) }, status: :ok
  end

  def create
    @comment = Comment.new(comment_params)

    return invalid_authentication unless comment_policy.create?

    if @comment.save
      render json: {
        status: 'Comment created successfully',
        comment: CommentSerializer.new(@comment)
      }, status: :created
    else
      render json: { errors: @comment.errors.full_messages }, status: :bad_request
    end
  end

  def update
    return invalid_authentication unless comment_policy.update?

    if @comment&.update_attributes(comment_params)
      render json: {
        status: 'Comment updated successfully',
        comment: CommentSerializer.new(@comment)
      }, status: :ok
    else
      render json: { errors: @comment&.errors&.full_messages }, status: :bad_request
    end
  end

  def destroy
    return invalid_authentication unless comment_policy.destroy?

    if @comment.destroy
      render json: {
        status: 'Comment deleted successfully'
      }, status: :ok
    else
      render json: {
        errors: @comment.errors.full_messages
      }, status: :unprocessable_entity
    end
  end

  private

  def set_comment
    @comment = Comment.find_by_id(comment_id)
    return not_found if @comment.blank?
  end

  def comment_id
    params[:id]
  end

  def paginate?
    params[:page].present? && params[:page].to_i.positive?
  end

  def comment_params
    params
      .require(:comment)
      .permit(:content, :commentable_id, :commentable_type, :list_id)
      .merge(user: @current_user)
  end

  def comment_policy
    @comment_policy ||= CommentPolicy.new(@current_user, @comment)
  end
end
