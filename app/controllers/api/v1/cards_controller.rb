# frozen_string_literal: true

# User controller
class Api::V1::CardsController < ApplicationController
  before_action :authenticate_request!
  before_action :set_card, except: %i[create index]

  def index
    cards = @current_user.cards
    render json: { cards: CardSerializer.new(cards) }, status: :ok
  end

  def show
    return invalid_authentication unless card_policy.show?

    render json: { card: CardSerializer.new(@card, params: { include: [:first_three_comments] }) }, status: :ok
  end

  def create
    @card = Card.new(card_params)

    return invalid_authentication unless card_policy.create?

    if @card.save
      render json: {
        status: 'Card created successfully',
        card: CardSerializer.new(@card)
      }, status: :created
    else
      render json: { errors: @card.errors.full_messages }, status: :bad_request
    end
  end

  def update
    return invalid_authentication unless card_policy.update?

    if @card&.update_attributes(card_params)
      render json: {
        status: 'Card updated successfully',
        card: CardSerializer.new(@card)
      }, status: :ok
    else
      render json: { errors: @card&.errors&.full_messages }, status: :bad_request
    end
  end

  def destroy
    return invalid_authentication unless card_policy.destroy?

    if @card.destroy
      render json: {
        status: 'Card deleted successfully'
      }, status: :ok
    else
      render json: {
        errors: @card.errors.full_messages
      }, status: :unprocessable_entity
    end
  end

  private

  def set_card
    @card = Card.find_by_id(card_id)
    return not_found if @card.blank?
  end

  def card_id
    params[:id]
  end

  def card_params
    params
      .require(:card)
      .permit(:title, :description, :list_id)
      .merge(created_user: @current_user)
  end

  def card_policy
    @card_policy ||= CardPolicy.new(@current_user, @card)
  end
end
