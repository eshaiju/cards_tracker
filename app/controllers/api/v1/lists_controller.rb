# frozen_string_literal: true

# User controller
class Api::V1::ListsController < ApplicationController
  before_action :authenticate_request!
  before_action :set_list, except: %i[create index]

  def index
    return invalid_authentication unless list_policy.index?

    lists = if @current_user.admin?
              @current_user.created_lists
            else
              @current_user.lists
            end
    render json: { lists: ListSerializer.new(lists) }, status: :ok
  end

  def show
    return invalid_authentication unless list_policy.show?

    render json: { list: ListSerializer.new(@list, params: { include: [:cards] }) }, status: :ok
  end

  def create
    return invalid_authentication unless list_policy.create?

    @list = List.new(list_params)

    if @list.save
      render json: {
        status: 'List created successfully',
        list: ListSerializer.new(@list)
      }, status: :created
    else
      render json: { errors: @list.errors.full_messages }, status: :bad_request
    end
  end

  def update
    return invalid_authentication unless list_policy.update?

    if @list&.update_attributes(list_params)
      render json: {
        status: 'List updated successfully',
        list: ListSerializer.new(@list)
      }, status: :ok
    else
      render json: { errors: @list&.errors&.full_messages }, status: :bad_request
    end
  end

  def destroy
    return invalid_authentication unless list_policy.destroy?

    if @list.destroy
      render json: {
        status: 'List deleted successfully'
      }, status: :ok
    else
      render json: {
        errors: @list.errors.full_messages
      }, status: :unprocessable_entity
    end
  end

  def assign_member
    return invalid_authentication unless list_policy.assign_member?

    assignment_service = AssignMemberService.new(@list, params)

    begin
      assignment_service.call
      render json: { status: 'Member assigned successfully' }, status: :ok
    rescue AssignMemberService::MemberDuplicateError => e
      render json: { errors: e.message }, status: :bad_request
    end
  end

  def un_assign_member
    return invalid_authentication unless list_policy.un_assign_member?

    un_assignment_service = UnAssignMemberService.new(@list, params)
    begin
      un_assignment_service.call
      render json: { status: 'Member un_assigned successfully' }, status: :ok
    rescue UnAssignMemberService::MemberNotExistError => e
      render json: { errors: e.message }, status: :not_found
    end
  end

  private

  def set_list
    @list = List.find_by_id(list_id)
    return not_found if @list.blank?
  end

  def list_id
    params[:id] || params[:list_id]
  end

  def list_params
    params
      .require(:list)
      .permit(:title)
      .merge(created_user: @current_user)
  end

  def list_policy
    @list_policy ||= ListPolicy.new(@current_user, @list)
  end
end
