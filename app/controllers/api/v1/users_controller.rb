# frozen_string_literal: true

# User controller
class Api::V1::UsersController < ApplicationController
  before_action :authenticate_request!, except: %i[create login]

  def index
    users = User.all
    render json: { users: UserSerializer.new(users) }
  end

  def create
    user = User.new(user_params)

    if user.save
      render json: { status: 'User created successfully', user: UserSerializer.new(user) }, status: :created
    else
      render json: { errors: user.errors.full_messages }, status: :bad_request
    end
  end

  def login
    user = User.find_by(username: params[:username].to_s)

    if user&.authenticate(params[:password])
      auth_token = JsonWebToken.encode(user_id: user.id)
      render json: { auth_token: auth_token, user: UserSerializer.new(user) }, status: :ok
    else
      render json: { error: 'Invalid username/password' }, status: :unauthorized
    end
  end

  private

  def user_params
    params
      .require(:user)
      .permit(:email, :password, :username, :role)
  end
end
