# frozen_string_literal: true

Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'

  namespace :api do
    namespace :v1 do
      resources :users, only: %i[create index] do
        collection do
          post 'login'
        end
      end
      resources :lists do
        post 'assign_member'
        post 'un_assign_member'
      end
      resources :cards, :comments
    end
  end
end
