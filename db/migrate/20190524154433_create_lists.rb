# frozen_string_literal: true

class CreateLists < ActiveRecord::Migration[5.2]
  def change
    create_table :lists do |t|
      t.string :title
      t.integer :created_user_id

      t.timestamps
    end

    add_index :lists, :created_user_id
  end
end
