# frozen_string_literal: true

class AddIndexToUserFields < ActiveRecord::Migration[5.2]
  def change
    add_index :users, :username
    add_index :users, :email
    add_index :users, :role
  end
end
