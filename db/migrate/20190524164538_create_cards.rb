# frozen_string_literal: true

class CreateCards < ActiveRecord::Migration[5.2]
  def change
    create_table :cards do |t|
      t.string :title
      t.text :description
      t.integer :list_id
      t.integer :created_user_id

      t.timestamps
    end

    add_index :cards, :list_id
    add_index :cards, :created_user_id
  end
end
