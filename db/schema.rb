# frozen_string_literal: true

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_190_526_174_933) do
  # These are extensions that must be enabled in order to support this database
  enable_extension 'plpgsql'

  create_table 'cards', force: :cascade do |t|
    t.string 'title'
    t.text 'description'
    t.integer 'list_id'
    t.integer 'created_user_id'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.integer 'comments_count', default: 0, null: false
    t.index ['created_user_id'], name: 'index_cards_on_created_user_id'
    t.index ['list_id'], name: 'index_cards_on_list_id'
  end

  create_table 'comments', force: :cascade do |t|
    t.text 'content'
    t.integer 'list_id'
    t.integer 'user_id'
    t.integer 'commentable_id'
    t.string 'commentable_type'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.integer 'comments_count', default: 0, null: false
    t.index %w[commentable_id commentable_type], name: 'index_comments_on_commentable_id_and_commentable_type'
    t.index ['list_id'], name: 'index_comments_on_list_id'
    t.index ['user_id'], name: 'index_comments_on_user_id'
  end

  create_table 'list_members', force: :cascade do |t|
    t.integer 'list_id'
    t.integer 'user_id'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['list_id'], name: 'index_list_members_on_list_id'
    t.index ['user_id'], name: 'index_list_members_on_user_id'
  end

  create_table 'lists', force: :cascade do |t|
    t.string 'title'
    t.integer 'created_user_id'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['created_user_id'], name: 'index_lists_on_created_user_id'
  end

  create_table 'users', force: :cascade do |t|
    t.string 'username'
    t.string 'email'
    t.string 'password_digest'
    t.string 'role'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['email'], name: 'index_users_on_email'
    t.index ['role'], name: 'index_users_on_role'
    t.index ['username'], name: 'index_users_on_username'
  end
end
